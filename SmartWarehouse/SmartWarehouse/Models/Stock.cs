﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace SmartWarehouse.Models
{
    public class Stock
    {
        [Key]
        public int ID { get; set; }
        public int ID_Item { get; set; }
        public DateTime Fecha { get; set; }

    }
}
